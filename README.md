**Requirements**  
To use `00-name` you need [figlet](http://www.figlet.org/) and [lolcat](https://github.com/busyloop/lolcat)

**Instalation**  
Run this command to pull the scripts and make them executable:
```
git clone https://gitlab.com/DavidPH/bash-message-of-the-day.git \
&& cd bash-message-of-the-day \
&& for i in ./*; do chmod +x "${i}"; done
```
Move the desired scripts to `/etc/update-motd/`. You can rename them to change the order, (e.g. `00-name` prints before `10-sysinfo`)

<img src="https://i.imgur.com/cGUqnvm.png">
