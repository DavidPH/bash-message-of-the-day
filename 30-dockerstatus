#!/bin/bash

cols=2
white="\e[1;39m"
green="\e[32;1m"
yellow="\e[33;1m"
red="\e[31;1m"
blue="\e[36;1m"
undim="\e[0m"

mapfile -t containers \
< <(docker ps -a --format='{{.Label "com.docker.compose.project"}},{{.Names}},{{.Status}}' \
| sort -r -k3 -t "," \
| awk -F '[ ,]' -v OFS=',' '{ print $1,$2,$3 }')

declare -A all_projects
declare -A all_containers
declare -A any_container_up
declare -A any_container_down

for container in "${containers[@]}"; do
    IFS="," read proj_name name status <<< ${container}
    if [ "$proj_name" != "" ]; then
        if  [ "${all_projects[$proj_name]}" == "" ]; then
            all_projects[$proj_name]=""
        fi
        all_projects[$proj_name]+="$name "
        if [ "$status" == "Up" ]; then
            any_container_up[$proj_name]=$status
        else
            any_container_down[$proj_name]=$status
        fi
    fi
    all_containers[$name]=$status
done;

declare -A top_output
declare -A bottom_output

for project_name in "${!all_projects[@]}"; do
    project=(${all_projects[$project_name]})
    if [ "${any_container_up[$project_name]}" != "" ] && [ "${any_container_down[$project_name]}" != "" ]; then
        status="partial"
        top_output[$project_name]="${blue}${project_name}:,${yellow}${status,,}${undim},"
        for container in "${project[@]}"; do
            status=${all_containers[$container]}
            if [ "$status" != "Up" ]; then
                bottom_output[$container]="${white}${container}:,${red}${status,,}${undim},"
            fi
            unset all_containers[$container]
        done;
    elif [ "${any_container_up[$project_name]}" != "" ]; then
        for container in "${project[@]}"; do
            status=${all_containers[$container]}
            unset all_containers[$container]
        done
        top_output[$project_name]="${blue}${project_name}:,${green}${status,,}${undim},"
    else 
        for container in "${project[@]}"; do
            status=${all_containers[$container]}
            unset all_containers[$container]
        done;
        bottom_output[$container]="${blue}${project_name}:,${red}${status,,}${undim},"
    fi
done;

for container in "${!all_containers[@]}"; do
    status=${all_containers[$container]}
    if [ "$status" != "Up" ]; then
        bottom_output[$container]="${white}${container}:,${red}${status,,}${undim},"
    else 
        top_output[$container]="${white}${container}:,${green}${status,,}${undim},"
    fi
done;

i=1
out=""
for name in "${top_output[@]}" "${bottom_output[@]}"; do 
    out+=$name
    if (( i % cols == 0 )); then
        out+="\n"
    fi
    ((++i))
done;
out+="\n"

printf "${white}docker status:\n"
printf "$out" | column -ts $',' | sed -e 's/^/  /'
printf "\n\n"